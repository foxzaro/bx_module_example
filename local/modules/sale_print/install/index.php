<?

IncludeModuleLangFile(__FILE__);
use \Bitrix\Main\ModuleManager;

Class Sale_Print extends CModule
{

    var $MODULE_ID = "sale_print";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $errors;

    function __construct()
    {
        $this->MODULE_VERSION = "1.0.0";
        $this->MODULE_VERSION_DATE = "2018-28-09";
        $this->MODULE_NAME = "Sale Print";
        $this->MODULE_DESCRIPTION = "desc";
    }

    function DoInstall()
    {
		global $APPLICATION;
		
        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();
        \Bitrix\Main\ModuleManager::RegisterModule("sale_print");
		$APPLICATION->IncludeAdminFile("Установка модуля Sale Print", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/sale_print/install/step.php");
        return true;
    }

    function DoUninstall()
    {
		global $APPLICATION;
		
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        \Bitrix\Main\ModuleManager::UnRegisterModule("sale_print");
		$APPLICATION->IncludeAdminFile("Деинсталляция модуля Sale Print", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/sale_print/install/unstep.php");
        return true;
    }

    function InstallDB()
    {
        global $DB;
        //$this->errors = false;
       // $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/atomdigital_yandexkassa/install/db/install.sql");
        //if (!$this->errors) {

          return true;
        //} else
          //  return $this->errors;
    }

    function UnInstallDB()
    {
        global $DB;
        //$this->errors = false;
        //$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/atomdigital_yandexkassa/install/db/uninstall.sql");
        //if (!$this->errors) {
            return true;
        //} else
          //  return $this->errors;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {
		
		//CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/sale_print/install/tools",
			//$_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/atomdigital_yandexkassa", true, true);
        return true;
    }

    function UnInstallFiles()
    {
		//DeleteDirFilesEx("/bitrix/tools/atomdigital_yandexkassa");
        return true;
    }
}