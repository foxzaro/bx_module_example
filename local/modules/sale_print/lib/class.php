<?

class CSalePrint{
	
	public static function test($topcount,$activeALL)
	{
		require($_SERVER["DOCUMENT_ROOT"].'/phpExcel/Classes/PHPExcel.php');
		CModule::IncludeModule("sale");
		if($activeALL=="Y"){
		$arFilter = array("ACTIVE"=>"Y");
		}
		else{
			$arFilter = array();	
		}
		$rsOrder = CSaleOrder::GetList(Array("PRICE"=>"DESC"), $arFilter, false, Array("nTopCount"=>$topcount));
		while($arOrder = $rsOrder->Fetch())
		{
			$arrayOrders[$arOrder["ID"]]["ID"] = mb_convert_encoding($arOrder["ID"], 'UTF-8', 'Windows-1251');
			$arrayOrders[$arOrder["ID"]]["USER_NAME"] = mb_convert_encoding($arOrder["USER_NAME"], 'UTF-8', 'Windows-1251');
			$arrayOrders[$arOrder["ID"]]["USER_LAST_NAME"] = mb_convert_encoding($arOrder["USER_LAST_NAME"], 'UTF-8', 'Windows-1251');
			$arrayOrders[$arOrder["ID"]]["USER_EMAIL"] =  mb_convert_encoding($arOrder["USER_EMAIL"], 'UTF-8', 'Windows-1251');
			$arrayOrders[$arOrder["ID"]]["PRICE"] = mb_convert_encoding($arOrder["PRICE"], 'UTF-8', 'Windows-1251');
			$arrayOrders[$arOrder["ID"]]["CURRENCY"] =  mb_convert_encoding($arOrder["CURRENCY"], 'UTF-8', 'Windows-1251');
		}

		$document = new \PHPExcel();
		$sheet = $document->setActiveSheetIndex(0); // Выбираем первый лист в документе
		
		$columnPosition = 0; // Начальная координата x
		$startLine = 2; // Начальная координата y
		
		// Вставляем заголовок в "A2" 
		$sheet->setCellValueByColumnAndRow($columnPosition, $startLine, 'Отчет по заказам');
		
		// Выравниваем по центру
		$sheet->getStyleByColumnAndRow($columnPosition, $startLine)->getAlignment()->setHorizontal(
			PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		// Объединяем ячейки "A2:C2"
		$document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition, $startLine, $columnPosition+6, $startLine);
		
		// Перекидываем указатель на следующую строку
		$startLine++;
		
		// Массив с названиями столбцов
		$columns = ["№",'ID заказа', 'Имя', 'Фамилия','E-mail','Сумма заказа','Валюта'];
		
		// Указатель на первый столбец
		$currentColumn = $columnPosition;
		
		// Формируем шапку
		foreach ($columns as $column) {
			// Тут можно покрасить ячейку
			// $sheet->getStyleByColumnAndRow($currentColumn, $startLine)
			//     ->getFill()
			//     ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
			//     ->getStartColor()
			//     ->setRGB('4dbf62');
		
			$sheet->setCellValueByColumnAndRow($currentColumn, $startLine, $column);
		
			// Смещаемся вправо
			$currentColumn++;
		}
		
		// Формируем список
		foreach ($arrayOrders as $key=>$catItem) {
			// Перекидываем указатель на следующую строку
			$startLine++;
			// Указатель на первый столбец
			$currentColumn = $columnPosition;
			// Вставляем порядковый номер
			$sheet->setCellValueByColumnAndRow($currentColumn, $startLine, $key+1);
		
			// Ставляем информацию об имени и цвете
			foreach ($catItem as $value) {
				$currentColumn++;
				$sheet->setCellValueByColumnAndRow($currentColumn, $startLine, $value);
			}
		}
		
		$objWriter = \PHPExcel_IOFactory::createWriter($document, 'Excel5');

		try {
			$objWriter->save($_SERVER["DOCUMENT_ROOT"]."/test/Report".date('m-d-Y h:i:s a', time()).".xls");
			return "1";
		} catch (Exception $e) {
			return "0";
		}

	}
	
}