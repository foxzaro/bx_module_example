<?
use Bitrix\Main\Localization\Loc;

if(!$USER->IsAdmin())
	return;

preg_match("/modules\/([a-zA-Z0-9_\.]+)\//", __FILE__, $find_module_id);
$moduleId = $find_module_id[1];
CModule::IncludeModule("sale_print");
$payment_options = [
	"1" => '1',
	"2" => '2',
	"3" => '3',
	"4" => '4',
	"5" => '5',
	"6" => '6',
	"7" => '7',
	"8" => '8',
	"9" => '9',
	"10" => '10',
];



$options_tab = [];

$options_tab[] = "��������� ��� ������������ �����";
$options_tab[] = array("PAYMENT_OPTION", '������� ������� ��������', null, array("selectbox", $payment_options));
//$options_tab[] = array("SEND_CHECK_DATA", Loc::GetMessage("TITLE_SEND_CHECK_DATA"), "N", array("checkbox", ""));
$options_tab[] = array("AUTO_CONFIRMATION", "�������� ������ ���������� ������", "Y", array("checkbox", ""));
// $options_tab[] = Loc::getMessage("MODULE_SETTINGS_AUTH_TITLE");
// $options_tab[] = array("SHOPID", Loc::GetMessage("TITLE_SHOPID"), null, array("text", 40));
// $options_tab[] = array("PASSWORD", Loc::GetMessage("TITLE_PASSWORD"), null, array("text", 40));
// $options_tab[] = Loc::getMessage("MODULE_SETTINGS_URL");
// $options_tab[] = array("RETURN_URL", Loc::GetMessage("TITLE_RETURN_URL"), null, array("text", 40));
//$options_tab[] = array("CALLBACK_URL", Loc::GetMessage("TITLE_CALLBACK_URL"), null, array("text", 40));

$aTabs = array(
    array(
        "DIV" => "options",
        "TAB" => '��������� ��� SalePrint',
		"TITLE" => '��������� ��� SalePrint',
        "OPTIONS" => $options_tab
    )
);

if ($_SERVER["REQUEST_METHOD"] == "POST" && strlen($_REQUEST["save"]) > 0 && check_bitrix_sessid())
{
    foreach ($aTabs as $aTab)
    {
        __AdmSettingsSaveOptions($moduleId, $aTab["OPTIONS"]);
    }

    LocalRedirect($APPLICATION->GetCurPage() . "?lang=" . LANGUAGE_ID . "&mid_menu=1&mid=" . urlencode($moduleId) .
        "&tabControl_active_tab=" . urlencode($_REQUEST["tabControl_active_tab"]) . "&sid=" . urlencode($siteId));
}
if($_POST["buttonDO"]){
	$perm = CSalePrint::test($_POST["PAYMENT_OPTION"],$_POST["AUTO_CONFIRMATION"]);
	if($perm=="1"){
		echo "<pre>"; print_r("����� �����������"); echo "</pre>";
	}
	else{
		echo "<pre>"; print_r("�������!"); echo "</pre>";
	}
}

$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>
<form method="post" action="" name="bootstrap">
	<? $tabControl->Begin();

	foreach ($aTabs as $aTab)
	{
		$tabControl->BeginNextTab();
		__AdmSettingsDrawList($moduleId, $aTab["OPTIONS"]);
	}

	$tabControl->Buttons(array("btnApply" => false, "btnCancel" => false, "btnSaveAndAdd" => false)); ?>
		<input class="mybutton" 
       type="submit" name="buttonDO" 
       value="������� �����" 
       onClick />
	<?= bitrix_sessid_post(); ?>
	<? $tabControl->End(); ?>
</form>

<?
echo BeginNote();
	echo "alpha version! Question? Write on <a href='mailto:foxzaro1@gmail.com'>email</a>";
echo EndNote();
?>