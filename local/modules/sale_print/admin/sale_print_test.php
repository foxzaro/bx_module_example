<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Application, 
	Bitrix\Main\Localization\Loc;
	
set_time_limit(0);

global $DB;

preg_match("/modules\/([a-zA-Z0-9_\.]+)\//", __FILE__, $find_module_id);
$module_id = $find_module_id[1];

$POST_RIGHT = $APPLICATION->GetGroupRight($module_id);

Loc::loadMessages(__FILE__);
CJSCore::Init(array("jquery")); 

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/prolog.php");

if($POST_RIGHT < "R")
{
    $APPLICATION->AuthForm(Loc::getMessage("FORBIDDEN"));
}

$request = Application::getInstance()->getContext()->getRequest(); 
$server = Application::getInstance()->getContext()->getServer();

$ID = intval($request->get('ID'));

$message = null;
$bVarsFromForm = false;

if($ID>0)
{
	$kassa = CAdKassa::GetByID($ID);
	if (!$kassa)
		$ID=0;
		
	foreach($kassa as $key=>$val)
	{
		$str = "str_".$key;
		global $$str;
		if ($val instanceof Bitrix\Main\Type\DateTime)
			$$str = $val->toString();
		else
			$$str = $val;
	}
}

if($bVarsFromForm)
	$DB->InitTableVarsForEdit("atomdigital_yandexkassa", "", "str_");

$APPLICATION->SetTitle("Оплата №".$ID);

require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");

$aMenu = array(
	array(
		"TEXT"=>"К списку оплат",
		"TITLE"=>"",
		"LINK"=>"atomdigital_yandexkassa_payments_list.php?lang=".LANG,
		"ICON"=>"btn_list",
	)
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

$aTabs = array(
	array("DIV" => "edit1", "TAB" => "Платеж", "TITLE"=>"Информация о платеже"),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);


$rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array());
while($arGr = $rsGroups->Fetch())
{
	$arGroups["REFERENCE"][] = $arGr["NAME"];
	$arGroups["REFERENCE_ID"][] = $arGr["ID"];
}
?>
<form method="POST" action="<?= $APPLICATION->GetCurPage()?>" name="discountform">
<?=bitrix_sessid_post()?>
<input type="hidden" name="ID" value=<?=$ID?>>
<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
<?if($request->get("adddiscount")<>""):?>
<input type="hidden" name="adddiscount" value="<?= htmlspecialcharsbx($request->get("adddiscount"))?>">
<?endif;?>
<?
$tabControl->Begin();
$tabControl->BeginNextTab();
$arStatuses = [
	"pending" => "<span class=\"pending-icon\"></span> Ожидает оплаты",
	"waiting" => "<span class=\"pending-icon\"></span> Ожидает подтверждения",
	"succeeded" => "<span class=\"succeeded-icon\"></span> Подтвержден",
	"canceled" => "<span class=\"canceled-icon\"></span> Отменен"
];
$payment_options = [
	"bank_card" => "Банковская карта"
];
?>
<?if($str_ID):?>
	<tr>
		<td>ID:</td>
		<td><?=intval($str_ID)?></td>
	</tr>
<?endif;?>
	<?if($str_TEST_MODE):?>
	<tr>
		<td><b>Тестовый платеж:</b></td>
		<td><b>Да</b></td>
	</tr>
	<?endif;?>
<!--	<tr>
		<td>Создание платежа:</td>
		<td><?=$str_CREATED_AT;?></td>
	</tr>-->
	<tr>
		<td>Статус оплаты:</td>
		<td><?=$arStatuses[$str_STATUS];?></td>
	</tr>
	<tr>
		<td>Номер заказа:</td>
		<td><?if(CModule::IncludeModule("ad_atomdigitalshop1")):?><a target="_blank" href="ad_atomdigitalshop1_edit.php?ID=<?=$str_ORDER_ID;?>&lang=<?=LANG?>">[<?=$str_ORDER_ID;?>]</a><?else:?><?=$str_ORDER_ID;?><?endif;?></td>
	</tr>
	<tr>
		<td>Номер заказа в Яндекс.Касса:</td>
		<td><?=$str_YANDEX_ORDER_ID;?></td>
	</tr>
	<tr>
		<td>Оплата:</td>
		<td><?=($str_PAID)?'<span style="color:green">Оплачен</span>':'<span style="color:red">Не оплачен</span>';?></td>
	</tr>
	<tr>
		<td>Сумма оплаты:</td>
		<td><?=$str_AMOUNT_VALUE;?></td>
	</tr>
	<tr>
		<td>Валюта оплаты:</td>
		<td><?=$str_AMOUNT_CURRENCY;?></td>
	</tr>
	<tr>
		<td>Ссылка на оплату:</td>
		<td><a target="_blank" href="<?=$str_CONFIRMATION_URL;?>"><?=$str_CONFIRMATION_URL;?></a></td>
	</tr>
	<?
	if(!empty($str_PAYMENT_METHOD)){
		?>
		<tr>
			<td>Способ оплаты:</td>
			<td><?=$payment_options[$str_PAYMENT_METHOD];?></td>
		</tr>
		<?
		if(!empty($str_CARD_FIRST6) && !empty($str_CARD_LAST4)){
		?>
		<tr>
			<td>Данные карты:</td>
			<td><span class="card-icon"></span><?=$str_CARD_FIRST6;?> ** **** <?=$str_CARD_LAST4;?></td>
		</tr>
		<?
		}
	}
	?>
	
<?
$tabControl->End();

?>
</form>
<link href="/bitrix/modules/<?=$module_id;?>/assets/admin_style.css" rel="stylesheet">
<?
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
?>
